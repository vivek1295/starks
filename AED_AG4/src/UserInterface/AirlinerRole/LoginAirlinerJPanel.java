/*
 * LoginSupplier.java
 */

package UserInterface.AirlinerRole;

import Business.Airliner;
import Business.AirlinerManager;
import Business.TravelAgency;
import java.awt.CardLayout;
import javax.swing.JPanel;



/**
 *
 * @author hp1
 */
public class LoginAirlinerJPanel extends javax.swing.JPanel {
    
    JPanel userProcessContainer;
    AirlinerManager airlinerManager;
    //Airliner a;
//    MasterOrderCatalog masterOrderCatalog;

    /** Creates new form LoginSupplier */
    public LoginAirlinerJPanel(JPanel upc,  AirlinerManager airlinerManager){
       initComponents();
       userProcessContainer = upc;
       this.airlinerManager = airlinerManager;
     //  this.a=a;
       
//       masterOrderCatalog = moc;
       airlinerDD.removeAllItems();
       for(Airliner airliner : airlinerManager.getAirlinerCatalog(this).getAirlinerCatalog()) {
           airlinerDD.addItem(airliner.getName());
       }
    }
    
    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        findButton1 = new javax.swing.JButton();
        jLabel2 = new javax.swing.JLabel();
        airlinerDD = new javax.swing.JComboBox();

        setBackground(new java.awt.Color(204, 204, 255));
        setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel1.setFont(new java.awt.Font("Book Antiqua", 1, 18)); // NOI18N
        jLabel1.setText("Airline Name");
        add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 170, -1, 30));

        findButton1.setBackground(new java.awt.Color(204, 204, 204));
        findButton1.setText("GO>>");
        findButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                findButton1ActionPerformed(evt);
            }
        });
        add(findButton1, new org.netbeans.lib.awtextra.AbsoluteConstraints(380, 170, -1, 30));

        jLabel2.setFont(new java.awt.Font("Book Antiqua", 1, 36)); // NOI18N
        jLabel2.setText("Airliner Login");
        add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(140, 50, -1, -1));

        airlinerDD.setBackground(new java.awt.Color(204, 204, 204));
        airlinerDD.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        airlinerDD.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                airlinerDDActionPerformed(evt);
            }
        });
        add(airlinerDD, new org.netbeans.lib.awtextra.AbsoluteConstraints(190, 170, 150, 30));
    }// </editor-fold>//GEN-END:initComponents

    private void findButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_findButton1ActionPerformed
        // TODO add your handling code here:
        String s = (String) airlinerDD.getSelectedItem();
        Airliner a = airlinerManager.getAirlinerCatalog(this).getAirlinerByName(s);
        ManageAirlineCatalogJPanel airlinerM = new ManageAirlineCatalogJPanel(userProcessContainer, airlinerManager, a);         
        userProcessContainer.add("ManageAirlineCatalogJPanels",airlinerM);
        CardLayout layout = (CardLayout)userProcessContainer.getLayout();
        layout.next(userProcessContainer);
    }//GEN-LAST:event_findButton1ActionPerformed

    private void airlinerDDActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_airlinerDDActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_airlinerDDActionPerformed
    
    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JComboBox airlinerDD;
    private javax.swing.JButton findButton1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    // End of variables declaration//GEN-END:variables
    
}
