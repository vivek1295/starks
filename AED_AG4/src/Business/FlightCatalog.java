/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.ArrayList;

/**
 *
 * @author Ankita
 */
public class FlightCatalog {
 
    ArrayList<Flight> flights;
    
    public FlightCatalog(){
        flights = new ArrayList<Flight>();
    }

    public ArrayList<Flight> getFlights() {
        return flights;
    }

    public void setFlights(ArrayList<Flight> flights) {
        this.flights = flights;
    }
    
    public Flight getFlightByNo(String flightNumber){
        
        for(int i = 0; i<flights.size(); i++){
            if(flights.get(i).getModelNumber().equals(flightNumber)) 
            {
                System.out.println("hello");
                return flights.get(i);
            }
       }
        return null;
    }
    
    
    
}
