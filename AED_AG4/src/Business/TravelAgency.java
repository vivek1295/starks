/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package Business;

import java.util.ArrayList;

/**
 *
 * @author Ankita
 */
public class TravelAgency {
    
    private String travelAgencyName;
    private AirlinerManager airlinerManager;
    private TravelOffice travelOffice;
    
    public TravelAgency() {
        airlinerManager = new AirlinerManager();
        travelOffice = new TravelOffice();
    }

    public String getTravelAgencyName() {
        return travelAgencyName;
    }

    public void setTravelAgencyName(String name) {
        this.travelAgencyName = name;
    }

    public Object getTravelAgencyReference(){
        return this;
    }
    @Override
    public String toString() {
        return travelAgencyName;
    }
    
    public void addAirliner(String name){
     airlinerManager.getAirlinerCatalog(this).addAirliner(name);
    }
    
     public void removeAirliner(Airliner p){
     airlinerManager.getAirlinerCatalog(this).removeAirliner(p);
    }
     
     public FlightCatalog showAllFlights(Airliner a){
         return airlinerManager.getAirlinerCatalog(this).searchAirliner(a).getFlightCatalog();      
     } 
     
     public AirlinerCatalog showAllAirliners(){
         return airlinerManager.getAirlinerCatalog(this);
     }
     
     public void removeAirliner(String travelAgencyName){
         airlinerManager.getAirlinerCatalog(this).removeAirlinerByName(travelAgencyName); 
     }
     
     public Airliner getAirliner(String travelAgencyName){
         return airlinerManager.getAirlinerCatalog(this).getAirlinerByName(travelAgencyName); 
     }
     
     public ArrayList<String[]> getFlightSchedule(){
         return airlinerManager.getFlightSchedule();
     }
     
     public AirlinerManager getAirlinerManager(){
         return airlinerManager;
     }
     
       public TravelOffice getTravelOffice(){
         return travelOffice;
     }
}
