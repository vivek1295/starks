/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.ArrayList;

/**
 *
 * @author Ankita
 */
public class Airliner {
    
    FlightCatalog flightCatalog;
    String name;
    
    
    public Airliner(String name){
        this.name = name;
        flightCatalog = new FlightCatalog();
    }
    
    public FlightCatalog getFlightCatalog() {
        return flightCatalog;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setFlightCatalog(FlightCatalog flightCatalog) {
        this.flightCatalog = flightCatalog;
    }
    
      public Flight getFlightByModelNumber(String number){
        ArrayList<Flight> flights = flightCatalog.getFlights();
        
        for(int i = 0; i<flights.size(); i++){
            Flight f = flights.get(i);
            if(String.valueOf(f.getModelNumber()).equals(number)) return f;
        }
        return null;
    }
    
}
