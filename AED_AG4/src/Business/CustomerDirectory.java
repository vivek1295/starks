/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.ArrayList;

/**
 *
 * @author Ankita
 */
public class CustomerDirectory {
    
    ArrayList<Person> customers;
   
    
    
    public CustomerDirectory()
            
    {
        customers=new ArrayList<Person>();
    } 

    public ArrayList<Person> getCustDir() {
        return customers;
    }

    public void setCustDir(ArrayList<Person> custDir) {
        this.customers= custDir;
    }
    
}
