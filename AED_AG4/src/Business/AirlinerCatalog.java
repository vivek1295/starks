/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package Business;

import java.util.ArrayList;

/**
 *
 * @author Ankita
 */
public class AirlinerCatalog {
    
    private ArrayList<Airliner> airliners;
    
    public AirlinerCatalog() {
        airliners = new ArrayList<Airliner>();
    }

    public ArrayList<Airliner> getAirlinerCatalog() {
        return airliners;
    }
    
    public Airliner addAirliner(String name) {
        Airliner p = new Airliner(name);
        airliners.add(p);
        return p;
    }
    
    public void removeAirliner(Airliner p) {
        airliners.remove(p);
    }
    
    public void removeAirlinerByName(String s){
        for(int i = 0; i<airliners.size(); i++){
            if(airliners.get(i).getName().equals(s)) airliners.remove(i);
        }
    }
    
     public Airliner getAirlinerByName(String s){
        for(int i = 0; i<airliners.size(); i++){
            if(airliners.get(i).getName().equals(s)) return airliners.get(i);
        }
        return null;
    }
    
    public Airliner searchAirliner(Airliner p) {
        int i = 0;
        for(i = 0; i<airliners.size(); i++){
            if(airliners.get(i) == p) break;
        }
        return airliners.get(i);
    }
    
  
    
}
