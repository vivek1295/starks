/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.awt.List;
import java.util.ArrayList;

/**
 *
 * @author Ankita
 */
public class FlightSchedule {
    
    private ArrayList<String[]> list;
    
    public FlightSchedule(){
        list = new ArrayList<String[]>();
    }
    
    public ArrayList<String[]> getFlightSchedule(AirlinerCatalog catalog){
        if(list.size()!= 0) return list;
        for(int i = 0; i<catalog.getAirlinerCatalog().size(); i++){
            for(int j = 0; j<catalog.getAirlinerCatalog().get(i).getFlightCatalog().getFlights().size(); j++){
                Flight f = catalog.getAirlinerCatalog().get(i).getFlightCatalog().getFlights().get(j);
                String a[] = new String[6];
                a[0] = catalog.getAirlinerCatalog().get(i).getName();
                a[1] = String.valueOf(f.getModelNumber());
                a[2] = f.getDestinationFrom();
                a[3] = f.getDestinationTo();
                a[4] = f.getDate().toString();
                a[5] = f.getTime();
                list.add(a);
        }
    }
    
            return list;   
}
 
       public String[] getFlightScheduleForEach(String s, AirlinerCatalog catalog){
                           String a[] = new String[6];
                           ArrayList<Airliner> list = catalog.getAirlinerCatalog();
        for(int i = 0; i<list.size(); i++){
            if(!list.get(i).getName().equals(s))continue;
            for(int j = 0; j<list.get(i).getFlightCatalog().getFlights().size(); j++){
                Flight f = list.get(i).getFlightCatalog().getFlights().get(j);
                a[0] = list.get(i).getName();
                a[1] = String.valueOf(f.getModelNumber());
                a[2] = f.getDestinationFrom();
                a[3] = f.getDestinationTo();
                a[4] = f.getDate().toString();
                a[5] = f.getTime();
                break;
        }
    }
    
            return a;   
}
    
    
}
