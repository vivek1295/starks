/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package Business;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Random;

/**
 *
 * @author Ankita
 */
public class Flight {
    
    private String modelNumber;
    private String airplaneName;
    private int price;
    private String destinationFrom;
    private String destinationTo;    
    private Date date;
    private String time;

    private HashMap<String, ArrayList<Boolean>> map;
    private ArrayList<Boolean> list;

   

    public Flight() {    
        initializeMap();
    }
    
    public void initializeMap(){
        if(this.map!=null)return;
        this.map = new HashMap<String, ArrayList<Boolean>>();
        for(char i = 'A'; i<='F'; i++){
               list = new ArrayList<Boolean>();
               for(int j = 1; j<=25; j++){
                  list.add(true);
               }
                map.put(String.valueOf(i), list);
        }
    }

    
     public HashMap<String, ArrayList<Boolean>> getMap() {
        return map;
    }

    public void setMap(HashMap<String, ArrayList<Boolean>> map) {
        this.map = map;
    }

     public String getModelNumber() {
        return modelNumber;
    }

    public void setModelNumber(String modelNumber) {
        this.modelNumber = modelNumber;
    }
   
    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }


    public String getDestinationFrom() {
        return destinationFrom;
    }

    public void setDestinationFrom(String destinationFrom) {
        this.destinationFrom = destinationFrom;
    }

    public String getDestinationTo() {
        return destinationTo;
    }

    public void setDestinationTo(String destinationTo) {
        this.destinationTo = destinationTo;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date dateTime) {
        this.date = dateTime;
    }
    
    @Override
    public String toString() {
        return airplaneName;
    }
    
}