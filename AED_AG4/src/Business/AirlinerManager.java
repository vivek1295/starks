/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.ArrayList;

/**
 *
 * @author Ankita
 */
public class AirlinerManager {
    
    private AirlinerCatalog airlinerCatalog;
    private FlightSchedule flightSchedule;
    
    public AirlinerManager(){
        airlinerCatalog = new AirlinerCatalog();
        flightSchedule = new FlightSchedule();
    }

    public AirlinerCatalog getAirlinerCatalog(Object obj) {
        return airlinerCatalog;
    }
    
    public ArrayList<String[]> getFlightSchedule(){
        return flightSchedule.getFlightSchedule(airlinerCatalog);
    }
    
    
    
    
    public String[] getFlightScheduleForEachAirliner(String s, AirlinerCatalog a){
        return flightSchedule.getFlightScheduleForEach(s, a);
    }
    
    public void addFlight(Flight f, String airlinerName){
        Airliner airliner = airlinerCatalog.getAirlinerByName(airlinerName);
        ArrayList<Flight> flights = airliner.getFlightCatalog().getFlights();
        flights.add(f);    
    }
    
    public void removeFlight(String flightName, String airlinerName){
        Airliner airliner = airlinerCatalog.getAirlinerByName(airlinerName);
        ArrayList<Flight> flights = airliner.getFlightCatalog().getFlights();
        Flight f = airliner.getFlightByModelNumber(flightName);
        flights.remove(f);
    } 
    
   public void updateFlightNumber(String flightName, String airlinerName, String newNumber){
    Airliner airliner = airlinerCatalog.getAirlinerByName(airlinerName);
        ArrayList<Flight> flights = airliner.getFlightCatalog().getFlights();
        Flight f = airliner.getFlightByModelNumber(flightName);
        f.setModelNumber(newNumber);
   }
   
   public void updateFlightDestinationTo(String flightName, String airlinerName, String newDest){
    Airliner airliner = airlinerCatalog.getAirlinerByName(airlinerName);
        ArrayList<Flight> flights = airliner.getFlightCatalog().getFlights();
        Flight f = airliner.getFlightByModelNumber(flightName);
        f.setDestinationTo(newDest);
   }
   
     public void updateFlightDestinationFrom(String flightName, String airlinerName, String newDest){
    Airliner airliner = airlinerCatalog.getAirlinerByName(airlinerName);
        ArrayList<Flight> flights = airliner.getFlightCatalog().getFlights();
        Flight f = airliner.getFlightByModelNumber(flightName);
        f.setDestinationFrom(newDest);
   }
    
    public void updateFlightTime(String flightName, String airlinerName, String time){
        Airliner airliner = airlinerCatalog.getAirlinerByName(airlinerName);
        ArrayList<Flight> flights = airliner.getFlightCatalog().getFlights();
        Flight f = airliner.getFlightByModelNumber(flightName);
        f.setTime(time);
   }
    public void updateFlightDate(String flightName, String airlinerName, String newDate){
        Airliner airliner = airlinerCatalog.getAirlinerByName(airlinerName);
        ArrayList<Flight> flights = airliner.getFlightCatalog().getFlights();
        Flight f = airliner.getFlightByModelNumber(flightName);
        //f.setDate(newDate);
   }
}
