/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

/**
 *
 * @author Ankita
 */
public class Seat {
    
    private int row;
    private String column;
    
    public Seat(int row, String column){
        this.row = row;
        this.column = column;
    }    

    public int getRow() {
        return row;
    }

    public void setRow(int row) {
        this.row = row;
    }

    public String getColumn() {
        return column;
    }

    public void setColumn(String column) {
        this.column = column;
    }
    
    
    
    
    
    
}
