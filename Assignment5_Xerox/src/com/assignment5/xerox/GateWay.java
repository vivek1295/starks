/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.assignment5.xerox;

import com.assignment5.analytics.AnalysisHelper;
import com.assignment5.analytics.DataStore;
import com.assignment5.entities.Customer;
import com.assignment5.entities.Item;
import com.assignment5.entities.Order;
import com.assignment5.entities.Product;
import java.io.IOException;
import com.assignment5.utils.SalesHelper;
import com.assignment5.entities.SalesPerson;
import java.util.*;
/**
 *
 * @author kasai
 */
public class GateWay {
    
    DataReader orderReader;
    DataReader productReader;
    AnalysisHelper helper = new AnalysisHelper();
    static List<Product> product = new ArrayList<>();
    static List<SalesPerson> salesPerson = new ArrayList<>();
    
    public GateWay() throws IOException {
        DataGenerator generator = DataGenerator.getInstance();
        orderReader = new DataReader(generator.getOrderFilePath());
        productReader = new DataReader(generator.getProductCataloguePath());
     
    }

    public static void main(String args[]) throws IOException {
        //Below is the sample for how you can use reader. you might wanna
        //delete it once you understood.
//        DataReader orderReader = new DataReader(generator.getOrderFilePath());
//        String[] orderRow;
//        DataReader productReader = new DataReader(generator.getProductCataloguePath());
//        String[] prodRow;
        GateWay inst = new GateWay();
        inst.readData();
        
//        List<Product> listproduct = new ArrayList<>();
//        //printRow(productReader.getFileHeader());
//        while ((prodRow = productReader.getNextRow()) != null) {
//            listproduct.add(getProduct(prodRow));
//        }
//        System.out.println("_____________________________________________________________");
//        
//        List<SalesPerson> list = new ArrayList<>();
//        //System.out.println("SalesPrice");
//        
//        //printRow(orderReader.getFileHeader());
//        while ((orderRow = orderReader.getNextRow()) != null) {
//            //printRow(prodRow);
//            list.add(getSalesPerson(orderRow));
//        }
       
       
         System.out.println("TOP 3 NEGOTIATED PRICE");
     System.out.println(SalesHelper.getTopSales(product,salesPerson));
     
     //Question 4//
     System.out.println("-----------------------------------------------");
     //SalesHelper.totalRevenueMarket();
    }

    public static void printRow(String[] row) {
        for (String row1 : row) {
        System.out.print(row1 + ", ");
        }
        System.out.println("");
    }
    public static Product getProduct(String[] row){
    int i=0,i2=0;
        for (int x = 0; x < row.length; x++) {
            switch (x) {
                case 0:
                    i=Integer.parseInt(row[x]);
                    break;
                case 3:
                    i2=Integer.parseInt(row[x]);
                    break;
                default:
                    break;
    }
        }
    return new Product(i,i2);
    }
    public static SalesPerson getSalesPerson(String[] row){
        int i=0,i2=0,i3=0,i4=0,i5=0,i6=0,i7=0;
        String y=""; 
        for (int x = 0; x < row.length; x++) {
            switch (x) {
                case 0:
                    i=Integer.parseInt(row[x]);
                    break;
                case 1:
                    i2=Integer.parseInt(row[x]);
                    break;
                case 2:
                    i3=Integer.parseInt(row[x]);
                    break;
                case 3:
                    i4=Integer.parseInt(row[x]);
                    break;
                case 4:
                    i5=Integer.parseInt(row[x]);
                    break;
                case 5:
                    i6=Integer.parseInt(row[x]);
                    break;
                case 6:
                    i7=Integer.parseInt(row[x]);
                    break;
                case 7:
                    y =row[x];
                    break;
                default:
                    break;

            }
        }
        //System.out.println(i+" "+i2+" "+i3+" "+i4+" "+i5+" "+i6+" "+i7+" "+y);
    return new SalesPerson(i6,new Order(i,i2,i6,new Item(i3,i7,i4)),y);
    }
    private void readData() throws IOException{
        String[] row;
        List<Product> productList = new ArrayList<>();
        List<SalesPerson> salesPersonList = new ArrayList<>();
        //System.out.println("SalesPrice");
        
        //printRow(orderReader.getFileHeader());
      
            
        while((row = productReader.getNextRow()) != null ){
             
            generateProduct(row);
            productList.add(getProduct(row));
            this.product = productList;
        }
        while((row = orderReader.getNextRow()) != null ){
            //Order order = generateOrder(row);
            salesPersonList.add(getSalesPerson(row));
            this.salesPerson = salesPersonList;
            Item item = generateItem(row);
            Order order = generateOrder(row, item);
            
   
            generateSalesPerson(row, order);
            generateCustomer(row, order);
        }
        runAnalysis();
        }

    private void runAnalysis() {
        helper.findTopThreeSalesPersonAndTotalRevenue();
        
        helper.findTopThreeCustomers();
    }
        
    private Item generateItem(String[] row) {
        int productId = Integer.parseInt(row[2]);
        int salesPrice = Integer.parseInt(row[6]);
        int quantity = Integer.parseInt(row[3]);
        int orderId = Integer.parseInt(row[0]);
        Item item = new Item(productId, orderId, salesPrice, quantity);
        return item;
    }
            
    private void generateSalesPerson(String[] row, Order order) {
        int salesPersonId = Integer.parseInt(row[4]);
       // List<Order> orderList = new ArrayList<>(orders.values());
        Map<Integer, SalesPerson> salesPersonMap = DataStore.getInstance().getSalesPerson(); 
        if(salesPersonMap.containsKey(salesPersonId)){
            salesPersonMap.get(salesPersonId).getOrderList().add(order);
        }else{
            SalesPerson salesPerson = new SalesPerson(salesPersonId);
            salesPerson.getOrderList().add(order);
            salesPersonMap.put(salesPersonId, salesPerson);
        }
    }
    
    private void generateCustomer(String[] row, Order order) {
        int customerId = Integer.parseInt(row[5]);
        Map<Integer, Customer> customersMap = DataStore.getInstance().getCustomers(); 
        if(customersMap.containsKey(customerId)){
            customersMap.get(customerId).getOrderList().add(order);
        }else{
            Customer customer = new Customer(customerId);
            customer.getOrderList().add(order);
            customersMap.put(customerId, customer);
        }
    }
    
    private void generateProduct(String[] row){
        int productId = Integer.parseInt(row[0]);
        int abc = Integer.parseInt(row[1]);
        int cde = Integer.parseInt(row[2]);
        Product prod = new Product(productId, abc, cde);
        DataStore.getInstance().getProduct().put(productId, prod);
    }
    
    private Order generateOrder(String[] row, Item item) {
        int orderId = Integer.parseInt(row[0]);
        int supplierId = Integer.parseInt(row[4]);
        int customerId = Integer.parseInt(row[5]);
        Map<Integer, Order> orders = DataStore.getInstance().getOrder(); 
        Order o = new Order(orderId, supplierId, customerId);
        if(orders.containsKey(orderId)){
            orders.get(orderId).getItemList().add(item);
        }else{
            o.getItemList().add(item);
            orders.put(orderId, o);
        }
        return o;
    }
}
