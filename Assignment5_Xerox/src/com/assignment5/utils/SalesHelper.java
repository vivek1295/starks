/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.assignment5.utils;

import com.assignment5.analytics.DataStore;
import com.assignment5.entities.Item;
import com.assignment5.entities.Order;
import com.assignment5.entities.Product;
import com.assignment5.entities.SalesPerson;
import java.util.*;

/**
 *
 * @author Vividha
 */

class ValueComparator implements Comparator {

    Map map;

    public ValueComparator(Map map) {
        this.map = map;
    }

    public int compare(Object keyA, Object keyB) {
        Comparable valueA = (Comparable) map.get(keyA);
        Comparable valueB = (Comparable) map.get(keyB);
        return valueB.compareTo(valueA);
    }
} 

public class SalesHelper {

    
    
    public static String getTopSales(List<Product> productlist,List<SalesPerson> saleslist){
    Map<Integer,Integer> productmap =new LinkedHashMap<>();
    Map<Integer,Integer> resultmap =new LinkedHashMap<>();
    for(Product x:productlist){
    productmap.put(x.getProductid(),x.getTargetprice());
    //System.out.println("prod id "+x.getProductid()+" productmap val:"+productmap.get(x.getProductid())+" targetprice"+x.getTargetprice());
    }
    for(SalesPerson sp:saleslist){
    int sales = sp.getOrder().getItem().getSalesPrice();
    int pid  = sp.getOrder().getItem().getProductId();
    //System.out.println(" productmap value:"+ productmap.get(pid) +" sales value:"+sales);
    int diff =(sales - (productmap.get(pid)));
    if(diff>0){
    resultmap.put(sp.getOrder().getItem().getQuantity(),diff);
    //System.out.println("Quantity:"+sp.getOrder().getItem().getQuantity()+" Diff:"+resultmap.get(sp.getOrder().getItem().getQuantity()));
    }
    }
    Map<Integer,String> fin = new LinkedHashMap<>();
    int max=0,ix=0;
    int maxid = 0;
    while(ix < 3){
        max=0;
        maxid=0;
    for(Integer f: resultmap.keySet()){
    if( max < resultmap.get(f))
    { 
        max=resultmap.get(f);
        maxid=f;
    }
    }
    String maxstring = "Quantity:"+maxid+" Negotiated Price Difference"+resultmap.get(maxid)+"\n";
    //System.out.println("First:"+maxid+" "+resultmap.get(maxid));
    int temp=resultmap.remove(maxid);
    fin.put(ix+1,"Quantity:"+maxid+" Negotiated Price Difference:"+temp);
   
    while(resultmap.containsValue(max)){
    
        for(Integer f: resultmap.keySet()){
    if(max==resultmap.get(f))
    {   
        //System.out.println("Inside");
        max=resultmap.get(f);
        maxid=f;
    }
    }
    
       //continue;
   
    //maxstring = maxstring+maxid+" "+resultmap.get(maxid)+"\n";
    //System.out.println("duplicate: "+maxstring);        
    fin.put(ix+1,maxstring+"Quantity:"+maxid+" Negotiated Price Difference:"+resultmap.remove(maxid).toString());
    }
    ix++;
        }
    String ret = "";
    for(Integer key:fin.keySet()){
        //System.out.println(ff+" "+fin.get(ff));
        ret=ret.concat(key+": "+fin.get(key)+"\n");
    }
        return ret;
    }
    
    /* revenue cal */
    
    
    

    
    public static void totalRevenueMarket(){
         
          HashMap<String, Integer> topCustomerCount = new HashMap<String, Integer>();
          Map<Integer, Order> order = DataStore.getInstance().getOrder();
           Map<Integer,Product> product = DataStore.getInstance().getProduct();
          
        for (Order o1 : order.values()) {
            
            int quantity = o1.getItem().getQuantity();
            int salesId = o1.getSupplierId();
            
            String market = o1.getMarketSeg();
            int salesPrice = o1.getItem().getSalesPrice();
            Item itemObj = o1.getItem();
            int productID = itemObj.getProductId();
            
            
            int minPrice = product.get(productID).getMin_price();
            int soldAmount = quantity * salesPrice;
            int basePrice = quantity * minPrice;
            int revenue = soldAmount - basePrice;
            
            if(topCustomerCount.containsKey(market))
            {
               
                int oldRevenue = topCustomerCount.get(market);
            
                revenue+=oldRevenue;
                topCustomerCount.put(market, revenue);
            }
            else
            {
                topCustomerCount.put(market, revenue);
            }

        }
        
         System.out.println("Total revenue made by each market segment is :  ");
          printTopRevenue(topCustomerCount,6);
          
          
          

     }
     
     
     
      public static void printTopRevenue(Map unsortedMap, int top){
        int count = 0;
        Map<String, Integer> topUsers = new HashMap<>();
        Map sortedMap = new TreeMap(new ValueComparator(unsortedMap));
        sortedMap.putAll(unsortedMap);
        topUsers = sortedMap;
        
        for (String prod : topUsers.keySet()) {
            if(count == top){
                break;
            }
            System.out.println("The total revenue of " + prod + " market is : " + topUsers.get(prod)); 
            count++;
            
        }
     }
      
}
