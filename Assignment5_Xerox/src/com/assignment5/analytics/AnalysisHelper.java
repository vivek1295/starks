/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.assignment5.analytics;

import com.assignment5.entities.Customer;
import com.assignment5.entities.Item;
import com.assignment5.entities.Order;
import com.assignment5.entities.Product;
import com.assignment5.entities.SalesPerson;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 *
 * @author dell pc
 */
public class AnalysisHelper {

    public void findTopThreeCustomers() {
        Map<Integer,Customer> customersMap = DataStore.getInstance().getCustomers();
        Map<Integer,Product> products = DataStore.getInstance().getProduct();
        Map<Integer,Double> profitMap = new HashMap<>();
        
        for (Customer customer : customersMap.values()) {
            double profit = 0;
            List<Order> orderList = customer.getOrderList();
            for (Order order : orderList) {
                List<Item> items = order.getItemList();
                for (Item item : items) {
                    int salePrice = item.getSalesPrice();
                    int quantity = item.getQuantity();
                    int productId = item.getProductId();
                    int targetPrice = products.get(productId).getTargetprice();
                    if (salePrice > targetPrice) {
                        profit = profit + ((salePrice - targetPrice) * quantity);
                    }
                }
            }
            profitMap.put(customer.getCustomerId(), profit);
        }
        Set<Map.Entry<Integer,Double>> entrySet = profitMap.entrySet();
        Comparator<Map.Entry<Integer,Double>> comparator = new Comparator<Map.Entry<Integer,Double>>() {
            @Override
            public int compare(Map.Entry<Integer, Double> entry1, Map.Entry<Integer,Double> entry2) {
                double value1 = entry1.getValue();
                double value2 = entry2.getValue();
                return Double.compare(value2, value1);
            }
        };

        List<Map.Entry<Integer,Double>> entryList = new ArrayList<>(entrySet);
        Collections.sort(entryList, comparator);
        System.out.println("Top three customers are: ");
        if (entryList.size() > 3) {
            for (int i = 0; i < 3; i++) {
                System.out.println("For Customer id " + entryList.get(i).getKey()
                        + " helped make profit worth: " + entryList.get(i).getValue());
            }
        }        
    }


    public void findTopThreeSalesPersonAndTotalRevenue(){
        Map<Integer,SalesPerson> salesPerson = DataStore.getInstance().getSalesPerson();
        Map<Integer,Product> products = DataStore.getInstance().getProduct();
        Map<Integer,Double> profitMap = new HashMap<>();
        
        for(SalesPerson s : salesPerson.values()){
            double profit = 0;
            List<Order> order =  s.getOrderList();
            for(Order o : order){
                List<Item> item = o.getItemList();
                for(Item i : item){
                    int salePrice = i.getSalesPrice();
                    int quantity = i.getQuantity();
                    int productId = i.getProductId();
                    int targetPrice = products.get(productId).getTargetprice();
                    if(salePrice > targetPrice){
                       profit = profit + ((salePrice - targetPrice) * quantity); 
                    }    
                }
            }
            profitMap.put(s.getSalesid(), profit);
        }
        
        Set<Map.Entry<Integer,Double>> entrySet = profitMap.entrySet();
        Comparator<Map.Entry<Integer,Double>> comparator = new Comparator<Map.Entry<Integer,Double>>() {
            @Override
            public int compare(Map.Entry<Integer,Double> entry1, Map.Entry<Integer,Double> entry2) {
                double value1 = entry1.getValue();
                double value2 = entry2.getValue();
                return Double.compare(value2, value1);
            }
        };

        List<Map.Entry<Integer, Double>> entryList = new ArrayList<>(entrySet);
        Collections.sort(entryList,comparator);
        System.out.println("\n" + "The 3 Best Sales Persons are: ");
        if(entryList.size() > 3){
        for(int i=0; i<3; i++){
          System.out.println("Sales Person with id " + entryList.get(i).getKey()
                + " has maximum profit which is: " + entryList.get(i).getValue()); 
        }
        } 
        System.out.println("--------------------------------------------------------------------------------------");
        

        //Question 4
        double totalprofit = 0;
        for(double p : profitMap.values()){
           totalprofit = totalprofit + p;
        }
        System.out.println("\n"+"The absolute income of the year above the expected target is : " +totalprofit + "\n");
    }
}
