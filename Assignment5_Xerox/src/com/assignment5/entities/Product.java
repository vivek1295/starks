/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.assignment5.entities;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author kasai
 */
public class Product {
    private int productid;
    private int targetprice;
    private int min_price;
    private int max_price;
    private List<Order> orders;

    
    public Product(int productid, int targetprice) {
        this.productid = productid;
        this.targetprice = targetprice;
        //this.orders = new ArrayList<>();
    }
    
    public Product (int productid, int min_price, int max_price ){
       this.productid = productid;
       this.min_price = min_price;
       this.max_price = max_price;
       this.orders = new ArrayList<>();
    }
    
    public int getProductid() {
        return productid;
    }

    public void setProductid(int productid) {
        this.productid = productid;
    }

    public int getTargetprice() {
        return targetprice;
    }

    public void setTargetprice(int targetprice) {
        this.targetprice = targetprice;
    }

    public int getMin_price() {
        return min_price;
    }

    public void setMin_price(int min_price) {
        this.min_price = min_price;
    }

    public int getMax_price() {
        return max_price;
    }

    public void setMax_price(int max_price) {
        this.max_price = max_price;
    }

    public List<Order> getOrders() {
        return orders;
    }

    public void setOrders(List<Order> orders) {
        this.orders = orders;
    }
    
    
    
    
    
}
